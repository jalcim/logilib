Les UAL élémentaires calculent sur des nombres entiers, et peuvent effectuer les opérations communes séparable en quatre groupes :

1) Les opérations arithmétiques : addition, soustraction, changement de signe, etc.
2) les opérations logiques : compléments à un, à deux, et, ou, ou-exclusif, non, non-et, etc.
3) les opérations de comparaisons : test d'égalité, supérieur, inférieur, et leur équivalents « ou égal ».
4) les opérations de décalages et rotations
