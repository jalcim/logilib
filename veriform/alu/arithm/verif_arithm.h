#ifndef __VERIFORM_ALU_ADD_SUB__
#define __VERIFORM_ALU_ADD_SUB__
void veriform_add_sub(unsigned int *results, unsigned int max_int, unsigned int sub,
                      unsigned int operandA, unsigned int operandB,
                      unsigned int cin);

#endif /* __VERIFORM_ALU_ADD_SUB__ */
