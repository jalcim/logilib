# Description

Logilib is high density and performance logic library developped on a fully structural way.
The objective is to provide a stdcell library for yosys, highly parameterizable.
In option we provide a POC python (amaranth wrapper) for direct using.
Its a piece of a more big environnement with a logilib/libcmos architecture on top of pdk wrapper.

# Build

## Synthesis (first stage)

Synthesis of design for provide final verilog/rtlil usable

```sh
./script/yosys.sh
```
Result files are on synth/

## Simulation

Build of simulation by running in the root folder

```sh
./script/icarus.sh
```
Result files are on bin/

## Synthesis

Build of synthesis by running in the root folder

```sh
make synthesis
```

## Co-simulation

Build of co-simulation library by running in the root folder

```sh
make
```
Result files are on build/

## Partial build

To build only a part of cosim you may use this kind of command from root of logilib

Example for build alu lib

```sh
touch cosim/alu && make cosim/alu/build
```


## Environment

### VERILATOR_TIME_INCREMENT

Set time increment on each verilator eval default 1000

### VCD_TRACE_ON

Enable generation of vcd trace files

### COMPILE_STATIC

If defined compile all libraries as static

### LOG_LEVEL

Change log level, allowed values are boost severity levels:

trace
debug
info
warning
error
fatal

### ARITHM_WIRES_LIST

Set list of wire numbers to generate default `1 2 3 4 7 8 16 32`
When changing you need to add corresponding macros in [gate_macros.h](cosim/primitive/gate/gate_macros.h) and includes in [gate_includes.h](cosim/primitive/gate/gate_includes.h)

### GATES_WAYS_LIST

Set list of wire numbers to generate default `2 3 4 7 8`
When changing you need to comment/uncomment corresponding macros in [gate_macros.h](cosim/primitive/gate/gate_macros.h) and includes in [gate_includes.h](cosim/primitive/gate/gate_includes.h)

