#ifndef __COSIM_ADDX_INCLUDES_H__
#define __COSIM_ADDX_INCLUDES_H__

#include "Vadd_1.h"
#include "VaddX_1.h"
#include "VaddX_2.h"
#include "VaddX_3.h"
#include "VaddX_4.h"
#include "VaddX_8.h"
#include "VaddX_16.h"
#include "VaddX_32.h"

#endif /* __COSIM_ADDX_INCLUDES_H__ */
